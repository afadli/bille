package com.afadli.etudes.bille;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

public class BilleView extends View implements SensorEventListener {

    private Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private Bitmap ballBitmap;
    private int imageWidth;
    private int imageHeight;
    private int axeX;
    private int axeY;


    public BilleView(Context context) {
        super(context);
    }

    // permet d'assemblet le context graphique dans le generateur d'interface graphiqe de mon activité
    public BilleView(Context context, AttributeSet attrset) {
        super(context, attrset);
    }

    @Override
    protected void onSizeChanged(int width, int height, int oldw, int oldh) {
        super.onSizeChanged(width, height, oldw, oldh);

        //charger l'image
        ballBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.bille);
        imageWidth = ballBitmap.getWidth();
        imageHeight = ballBitmap.getHeight();

        axeX = (width - imageWidth) / 2;
        axeY = (height - imageHeight) / 2;

    }

    // methode invoquée lors de l'acctualisation de l'ecran
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawBitmap(ballBitmap, axeX, axeY, paint);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    // capturer les nouvelles informations sur l'axe des X, Y, et Z(hauteur)
    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        float x = sensorEvent.values[0];
        float y = sensorEvent.values[1];

        axeX += -x * 5;
        axeY += y * 5;

        Log.d("ASSIA", "X=" + x + " , Y=" + y);

        if (axeX < 0) {
            axeX = 0;
        } else
        if (axeX + imageWidth > getWidth()) {
            axeX = getWidth() - imageWidth;
        }

        if (axeY < 0) {
            axeY = 0;
        } else
        if (axeY + imageHeight > getHeight()) {
            axeY = getHeight() - imageHeight;
        }


        invalidate();

    }
}
package com.afadli.etudes.bille;

import android.annotation.SuppressLint;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;


public class MainActivity extends AppCompatActivity {

    private BilleView view;
    private SensorManager smgr = null;

    //@SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        LinearLayout box=findViewById(R.id.box);
        view=new BilleView(this);
        box.addView(view);
        //view = findViewById(R.id.billeView);
        smgr = (SensorManager) getSystemService( SENSOR_SERVICE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // on enregistre le listnner
        smgr.registerListener ( view, smgr.getDefaultSensor( Sensor.TYPE_ACCELEROMETER), smgr.SENSOR_DELAY_GAME);
    }

    @Override
    protected void onPause() {
        super.onPause();
        smgr.unregisterListener(view);
    }
}
